package com.fcm.krypton_bank.Controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fcm.krypton_bank.Entity.Employee;
import com.fcm.krypton_bank.Exception.EmailAlreadyExistException;
import com.fcm.krypton_bank.Repository.EmployeeRepository;
import com.fcm.krypton_bank.Service.CustomerService;
import com.fcm.krypton_bank.Service.EmployeeService;

import jakarta.validation.Valid;

@RestController()
@RequestMapping("/api/v2")
public class EmployeeController {
	private static final Logger logger = LogManager.getLogger(EmployeeController.class);
	@Autowired
	private EmployeeService employeeService;

	public EmployeeController(EmployeeService employeeService) {
		super();
		this.employeeService = employeeService;
	}

	@GetMapping("/employee")
	public List<Employee> getAllemp() {
		return employeeService.getAllEmployees();
	}

	@GetMapping("/employee/{id}")
	private Employee fetchBasedOnfindById(@PathVariable("id") int empId)  {
		
		return employeeService.findById(empId);
	}

	@PostMapping("/employee/add")
	public ResponseEntity<String> addEmployee(@Valid @RequestBody Employee employee) throws Exception {
		//try {
			employeeService.save(employee);
			String successMessage="Employee added successfully";
			return ResponseEntity.ok(successMessage);
//		}catch(EmailAlreadyExistException e) {
//			String msg=("Email already exists: " + e.getMessage());
//			
//			return ResponseEntity.ok(msg);
//		}
				
	}

	@DeleteMapping("/delete/{empId}")
	private void deleteEmployee(@PathVariable("empId") int empId) {
		employeeService.delete(empId);
	}
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<String> handleValidationException(MethodArgumentNotValidException ex){
		
		String errorMessage=ex.getBindingResult().getFieldError().getDefaultMessage();
		System.out.println(errorMessage);
		return ResponseEntity.badRequest().body(errorMessage);
		
	}
}
