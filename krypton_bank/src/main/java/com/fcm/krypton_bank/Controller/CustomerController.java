package com.fcm.krypton_bank.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fcm.krypton_bank.Entity.Customer;
import com.fcm.krypton_bank.Service.CustomerService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/customer")
public class CustomerController {
	@Autowired
	private CustomerService customerService;
	
	@PostMapping("/add")
	public ResponseEntity<String> addUser(@Valid @RequestBody Customer newCustomer)  
	{    
		customerService.save(newCustomer);  
		String successMessage="Customer added successfully";
		return ResponseEntity.ok(successMessage);
		
	} 

	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<String> handleValidationException(MethodArgumentNotValidException ex){
		
		String errorMessage=ex.getBindingResult().getFieldError().getDefaultMessage();
		System.out.println(errorMessage);
		return ResponseEntity.badRequest().body(errorMessage);
		
	}
}
