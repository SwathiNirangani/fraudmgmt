package com.fcm.krypton_bank.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fcm.krypton_bank.Entity.Customer;
import com.fcm.krypton_bank.Entity.Employee;
import com.fcm.krypton_bank.Entity.EmployeeType;
import com.fcm.krypton_bank.Service.EmployeeTypeService;

@RestController
@RequestMapping("/api/employeetype")
public class EmployeeTypeController {
	@Autowired
	private EmployeeTypeService employeeTypeService;

	@GetMapping("/{id}")
	
	private EmployeeType fetchBasedOnfindById(@PathVariable("id") int Id) {
		return employeeTypeService.getReviewers(Id);
	}
	
	@PostMapping("/add")
	public void addEmployeeType(@RequestBody EmployeeType employeeType)  
	{    
		employeeTypeService.save(employeeType);    
	} 
}
