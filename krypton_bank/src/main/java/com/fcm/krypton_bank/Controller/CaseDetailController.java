package com.fcm.krypton_bank.Controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.fcm.krypton_bank.Entity.CaseDetail;
import com.fcm.krypton_bank.Entity.Customer;
import com.fcm.krypton_bank.Entity.Employee;
import com.fcm.krypton_bank.Exception.CustomerIdIsNotFound;
import com.fcm.krypton_bank.Exception.StatusNotFoundException;
import com.fcm.krypton_bank.Service.CaseDetailService;
import com.fcm.krypton_bank.Service.CustomerService;

@RestController
@RequestMapping("/api/casedetail")
public class CaseDetailController {
	private static final Logger logger = LogManager.getLogger(CustomerService.class);

	@Autowired
	private CaseDetailService caseDetailService;

	@GetMapping("/case")
	public List<CaseDetail> getAllCaseDetails() {
		return caseDetailService.getAllCaseDetails();
	}
	@GetMapping("/{id}")
	private CaseDetail fetchBasedOnfindById(@PathVariable("id") int caseId)  {
		
		return caseDetailService.getCaseDetailById(caseId);
	}

	@PostMapping("/addcase")
	public ResponseEntity<String> addCase(@RequestBody CaseDetail caseDetails) {
		caseDetailService.save(caseDetails);
		String successMessage="Case added successfully";
		return ResponseEntity.ok(successMessage);
		
	}

	@GetMapping("/details/{customerId}")
	public List<CaseDetail> getAllCaseDetailByCustomerId(@PathVariable("customerId") int customerId) {
	
	 return caseDetailService.getCaseDetailCustomerById(customerId);
	}


	
	@GetMapping("/allCaseDetails/{caseDate}")
	public List<CaseDetail> getAllCaseDetailByCasedate(@PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") Date caseDate) {
		
			return caseDetailService.getCaseDetailByCaseDate(caseDate);
	
	
	}
	

	@GetMapping("/details/byemployeeid/{empId}")
	public List<CaseDetail> getAllCaseDetailByEmployeeId(@PathVariable("empId") int empId) {
		Employee employee = new Employee();
		employee.setEmpId(empId);
		return caseDetailService.getCaseDetailEmployeeById(employee);
	}
	
	
	@PutMapping("/{empId}")

    public CaseDetail updateCaseDetailAfterAssignEmployee(@PathVariable int empId) {

        return caseDetailService.updateCaseDetail(empId);

    }
	@PutMapping("/status/{empId}")

    public CaseDetail updateCaseDetailAfterAssignation(@PathVariable int empId) {

        return caseDetailService.updateCaseDetailAfterAssignation(empId);

    }
//	@PutMapping("/update/{caseId}")
//	public CaseDetail update(Integer caseId) {
//		return caseDetailService.update(caseId);
//	}
	
	@PutMapping("/update/empId/{empId}/caseId/{caseId}")
	public CaseDetail employeeIdUpdateCaseDetails(@PathVariable("empId") Integer empId, @PathVariable("caseId") Integer caseId) {
		CaseDetail employeeIdUpdateCaseDetails = caseDetailService.employeeIdUpdateCaseDetails(empId, caseId);
		return employeeIdUpdateCaseDetails;
		
	}
	
	@GetMapping("/bystatus/{status}")
	public List<CaseDetail> getCaseDetailsByStatus(@PathVariable("status") String status){
		return caseDetailService.getCaseDetailsByStatus(status);
	}
//    public List<CaseDetail> getCaseDetailsByStatus(@PathVariable("status") String status) {
//        try {
//            if (!isValidStatus(status)) {
//                throw new IllegalArgumentException("Invalid status:" + status);
//            }
//            logger.info("Fetching cases by status: {}", status);
//            List<CaseDetail> cases = caseDetailService.getCaseDetailsByStatus(status);
//
// 
//
//            if (cases.isEmpty()) {
//                throw new StatusNotFoundException("No cases found for status:" + status);
//            }
//            logger.info("Fetched {} cases with status: {}", cases.size(), status);
//            return cases;
//
// 
//
//        } catch (StatusNotFoundException e) {
//            logger.error("An exception occured while fetching cases:");
//            throw e;
//
// 
//
//        } catch (IllegalArgumentException e) {
//            logger.error("An exception occured while fetching cases:", e);
//            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
//
// 
//
//        } catch (Exception e) {
//            logger.error("An exception occured while fetching cases:", e);
//            throw new RuntimeException("Failed to fetch cases. Please try again", e);
//        }
//
// 
//
 //   }
    private boolean isValidStatus(String status) {
        return status.equalsIgnoreCase("New")||status.equalsIgnoreCase("inProgress")||status.equalsIgnoreCase("Completed")||status.equalsIgnoreCase("Reject");
    }
	
	
	
}
