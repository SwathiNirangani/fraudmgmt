package com.fcm.krypton_bank.Service;

import java.time.LocalDate;

import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.fcm.krypton_bank.Entity.CaseDetail;
import com.fcm.krypton_bank.Entity.Customer;
import com.fcm.krypton_bank.Entity.Employee;
import com.fcm.krypton_bank.Exception.CustomerIdIsNotFound;
import com.fcm.krypton_bank.Exception.DateAlreadyExistsException;
import com.fcm.krypton_bank.Exception.DateIsNotFoundException;
import com.fcm.krypton_bank.Exception.EmployeeIdIsNotFound;
import com.fcm.krypton_bank.Exception.StatusNotFoundException;
import com.fcm.krypton_bank.Repository.CaseDetailRepository;
import com.fcm.krypton_bank.Repository.EmployeeRepository;

@Service
public class CaseDetailService {
	private static final Logger logger = LogManager.getLogger(CustomerService.class);
	@Autowired
	private CaseDetailRepository caseDetailRepo;

	@Autowired
	private EmployeeRepository employeeRepository;

	public void save(CaseDetail caseDetail) {

//		Optional<CaseDetail> existing = caseDetailRepo.findByCaseDate(caseDetail.getCaseDate());
//		if (existing.isPresent()) {
//			throw new DateAlreadyExistsException("Case date already exists");
//		} else {
		caseDetail.setCaseDate(new Date());
		boolean cardCancelled = caseDetail.getCardCancelled().equalsIgnoreCase("yes");
		caseDetail.setCardCancelled(cardCancelled ? "y" : "n");

		boolean possessionWhenFraudOccured = caseDetail.getPossessionWhenFraudOccured().equalsIgnoreCase("yes");
		caseDetail.setPossessionWhenFraudOccured(possessionWhenFraudOccured ? "y" : "n");

		boolean pinShared = caseDetail.getPinShared().equalsIgnoreCase("yes");
		caseDetail.setPinShared(pinShared ? "y" : "n");
		caseDetail.setCardNumber(maskDigits(caseDetail.getCardNumber(), 12, 15));
		caseDetail.setAccountNumber(maskDigits(caseDetail.getAccountNumber(),
				caseDetail.getAccountNumber().length() - 4, caseDetail.getAccountNumber().length() - 1));

		caseDetailRepo.save(caseDetail);
		
		// }
	}
	
	public CaseDetail employeeIdUpdateCaseDetails(Integer empId, Integer caseId) {
		CaseDetail caseDetail = null;
		Employee empfindById = employeeRepository.findById(empId).get();
		CaseDetail casefindById = caseDetailRepo.findById(caseId).get();
		casefindById.setEmployee(empfindById);
		caseDetail = caseDetailRepo.save(casefindById);
		return caseDetail;
		
	}

	private String maskDigits(String input, int start, int end) {
		StringBuilder maskedInput = new StringBuilder(input);
		for (int i = 0; i < start; i++) {
			maskedInput.setCharAt(i, '*');
		}
		return maskedInput.toString();
	}

	public List<CaseDetail> getAllCaseDetails() {
		List<CaseDetail> caseDetails = new ArrayList<>();
		CaseDetail casedetail = new CaseDetail();
		caseDetails = caseDetailRepo.findAll();
		return caseDetails;
	}

	public List<CaseDetail> getCaseDetailCustomerById(int customerId) {// Customer customer)

		try {
			List<CaseDetail> existing = caseDetailRepo.findByCustomerCustomerId(customerId);
			if (existing.isEmpty()) {
				throw new CustomerIdIsNotFound("Customer Id is not Found");
			}
			return existing;
		} catch (CustomerIdIsNotFound e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}

	public List<CaseDetail> getCaseDetailByCaseDate(Date caseDate) {
		// CaseDetail caseDetail = new CaseDetail();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDate localDate = caseDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		String formattedDate = localDate.format(formatter);
		System.out.println("Formatted date: " + formattedDate);
		Date parsedDate = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
		try {
			List<CaseDetail> existing = caseDetailRepo.findByCaseDate(parsedDate);
			if (existing.isEmpty())
				throw new DateIsNotFoundException("CaseDate is not found");
			else
				return existing;
		} catch (DateIsNotFoundException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}

	}

	public List<CaseDetail> getCaseDetailEmployeeById(Employee employee) {
		try {
			List<CaseDetail> existing = caseDetailRepo.findByEmployee(employee);
			if (existing.isEmpty()) {
				throw new EmployeeIdIsNotFound("Employee Id is not Found");
			}
			return existing;
		} catch (EmployeeIdIsNotFound e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());

		}
	}

	public CaseDetail getCaseDetailById(int caseId) {
		Optional<CaseDetail> caseDb = caseDetailRepo.findById((int) caseId);
		return caseDb.get();
	}

	public CaseDetail updateCaseDetail(int empId) {

		try {
			Employee employee = new Employee();
			employee.setEmpId(empId);
			CaseDetail caseDetail = caseDetailRepo.findByEmployee(employee).get(0);
			// set the reviewerType
          
			if (caseDetail.getTransactionAmount() > 1000)
				caseDetail.setReviewerType("Reviewer");
			else
				caseDetail.setReviewerType("FraudcaseOfficer");
			// save the updated case detail
			return caseDetailRepo.save(caseDetail);
		} catch (IndexOutOfBoundsException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}

	public CaseDetail updateCaseDetailAfterAssignation(int empId) {
		try {
			CaseDetail cs = updateCaseDetail(empId);
			if (cs.getTransactionType().equals("DebitCard")) {
				cs.setStatus("In progress");
				cs.setCasePossibility("Accept");
			} else {
				cs.setStatus("Reject");
				cs.setCasePossibility("Reject");
			}
			return caseDetailRepo.save(cs);
		} catch (IndexOutOfBoundsException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}

	}
	
	
    public List<CaseDetail> getCaseDetailsByStatus(String status) {
    	try {
			List<CaseDetail> existing = caseDetailRepo.findByStatus(status);
			if (existing.isEmpty()) {
				throw new StatusNotFoundException("Customer Id is not Found");
			}
			return existing;
		} catch (StatusNotFoundException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
        
    }
    
    
 

}
