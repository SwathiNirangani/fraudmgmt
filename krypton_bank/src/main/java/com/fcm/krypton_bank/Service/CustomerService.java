package com.fcm.krypton_bank.Service;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.apache.logging.log4j.Logger;

import com.fcm.krypton_bank.Entity.Customer;
import com.fcm.krypton_bank.Repository.CustomerRepository;

@Service
public class CustomerService {
    private static final Logger logger = LogManager.getLogger(CustomerService.class);

	@Autowired
	CustomerRepository customerRepo;
	PasswordEncoder passwordEncoder;
	public Customer save(Customer customer) {
		
		this.passwordEncoder = new BCryptPasswordEncoder();
        String encodedPassword = this.passwordEncoder.encode(customer.getPassword());
        logger.info(encodedPassword);
        customer.setPassword(encodedPassword);
        customer = customerRepo.save(customer);
        return customer;
    }
   
}
