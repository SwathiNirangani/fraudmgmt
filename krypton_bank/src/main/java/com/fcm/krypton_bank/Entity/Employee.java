package com.fcm.krypton_bank.Entity;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "Employee_Detail")

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class,property="empId") 
public class Employee {
	@Id
	@Column(name = "EMPLOYEE_ID", length = 10)
	//@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int empId;

	@Column(name = "FIRST_NAME", length = 250)
	@NotEmpty(message="first name connot be empty")
	private String firstName;

	@Column(name = "MIDDLE_NAME", length = 250)
	// @Nonnull
	private String middleName;

	@Column(name = "LAST_NAME", length = 250)
	@NotEmpty(message="last name connot be empty")
	private String lastName;

	@Column(name = "EMAIL", length = 250)
	@NotEmpty(message="Email connot be empty")
	@Email(message = "Email is not valid", regexp = "^[a-z0-9]+@[a-z]+\\.[a-z]{2,3}")
	private String email;

	@Column(name = "PHONE_NO",length=10)
	@NotNull(message="Phone number connot be empty")
	//@Range(min = 10, max = 10,message="phone number must be exactly same")
	//@Pattern(regexp="^\\\\d{10,10}$")
	private Long phoneno;

	@Column(name = "PASSWORD", length = 250)
	@NotEmpty(message="password connot be empty")
	@Pattern(regexp = "^(?=.*[A-Za-z])(?=.*\\d)(?=.*[@$!%*#?&])[A-Za-z\\d@$!%*#?&]{8,}$",message="password is not valid")
	private String password;

	@Column(name = "GENDER", length = 250)
	@NotEmpty(message="Gender can not be empty")
	private String gender;

	@Column(name = "EMPLOYEE_TYPE", length = 250)
	@NotEmpty(message="EmployeeType connot be empty")
	private String employeeType;
	
	@Column(name = "NO_OF_CASE_HANDLING", length = 250)
	//@NotEmpty(message="Number of caseHandling connot be empty")
	private int noOfCasesHandling;
	
	//@JsonManagedReference
	//@JsonBackReference
	@JsonIdentityReference(alwaysAsId = true)
	@OneToMany(mappedBy = "employee" ,fetch = FetchType.LAZY,cascade = CascadeType.ALL)//, cascade = CascadeType.ALL
	private List<CaseDetail> caseDetail;


	 
}
