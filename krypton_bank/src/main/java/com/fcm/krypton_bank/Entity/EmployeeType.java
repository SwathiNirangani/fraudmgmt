package com.fcm.krypton_bank.Entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="EmployeeType")
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeType {
	@Id
	private int id;
	private String reviewer;
	private String approver;
	

}
