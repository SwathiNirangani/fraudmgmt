package com.fcm.krypton_bank.Entity;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Entity
@Table(name="Customer_Detail")
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="customerId")

public class Customer {
	  @Id 
	   @GeneratedValue(strategy = GenerationType.IDENTITY)
	  @Column(name="CUSTOMER_ID" ,unique=true)
	   private int customerId;
	  
	  @Column(name="FIRST_NAME")
	  @NotEmpty(message="first name connot be empty")
	   private String firstName;
	  
	  @Column(name="MIDDLE_NAME")
	  //(message="first name connot be empty")
	  private String middleName;
	  
	  @Column(name="LAST_NAME")
	  @NotEmpty(message="last name connot be empty")
	   private String lastName;
	  
	  @Column(name="EMAIL")
	  @NotEmpty(message="Email connot be empty")
	  //@Email(message = "Email is not valid", regexp = "[a-z0-9]+@[a-z]+\\\\.[a-z]{2,3}")
	  @Email(regexp = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$",message="Email is not valid")
	   private String email;
	  
	  @Column(name="PHONE_NUMBER")
	  @NotNull(message="Phone number connot be empty")
	  @Min(10)
	 // @Max(10)
	   private Long phoneNumber;
	  
	  @Column(name="GENDER")
	  @NotEmpty(message="Gender connot be empty")
	   private String gender;
	  
	  @Column(name="ADDRESS")
	  @NotEmpty(message="Address connot be empty")
	   private String address;
	   
	  @Column(name="PAN_CARD")
	  @NotEmpty(message="Pan card cannot be empty")
	 // @Pattern(regexp="^[A-Z]{5}[CHFATBLJGP][A-Z][0-9]{4}[A-Z]$",message="Enter valid pan card number")
	  @Pattern(regexp="^[A-Z]{5}[0-9]{4}[A-Z]{1}$",message="Enter valid pan card number")
	  private String panCard;
	  
	  @Column(name="PASSWORD")
	  @NotEmpty(message="password connot be empty")
	  @Pattern(regexp = "^(?=.*[A-Za-z])(?=.*\\d)(?=.*[@$!%*#?&])[A-Za-z\\d@$!%*#?&]{8,}$",message="password is not valid")
	  private String password;

	   @JsonManagedReference
	   @OneToMany(mappedBy="customer")
	   private List<CaseDetail> caseDetails;
	   

}
