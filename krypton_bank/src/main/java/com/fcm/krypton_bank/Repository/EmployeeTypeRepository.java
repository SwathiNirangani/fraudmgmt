package com.fcm.krypton_bank.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.fcm.krypton_bank.Entity.EmployeeType;

@Repository
public interface EmployeeTypeRepository extends JpaRepository<EmployeeType,Integer> {

	//List<EmployeeType> findByReviewer();

}
