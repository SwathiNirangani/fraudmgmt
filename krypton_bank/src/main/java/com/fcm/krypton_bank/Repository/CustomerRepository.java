package com.fcm.krypton_bank.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.fcm.krypton_bank.Entity.Customer;
@Repository
public interface CustomerRepository extends JpaRepository<Customer, Integer>{
 
}
