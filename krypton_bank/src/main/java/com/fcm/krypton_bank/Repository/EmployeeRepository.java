package com.fcm.krypton_bank.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.fcm.krypton_bank.Entity.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee,Integer> {
 Employee findByEmployeeType(String employeeType);

   Employee findByEmail(String email);
}
