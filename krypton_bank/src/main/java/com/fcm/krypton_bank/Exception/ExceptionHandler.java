package com.fcm.krypton_bank.Exception;

import org.springframework.web.context.request.WebRequest;

public class ExceptionHandler {
	@org.springframework.web.bind.annotation.ExceptionHandler(value = {EmailAlreadyExistException.class})
    public ExceptionDetails handleEmailAlreadyExistsException(EmailAlreadyExistException e, WebRequest req) {
        ExceptionDetails ed = new ExceptionDetails();
        ed.setMessage(e.getMessage());
        ed.setUrl(req.getDescription(false));
        return ed;
    }
	
	@org.springframework.web.bind.annotation.ExceptionHandler(value = {DateAlreadyExistsException.class})
    public ExceptionDetails handleDateAlreadyExistsException(DateAlreadyExistsException e, WebRequest req) {
        ExceptionDetails ed = new ExceptionDetails();
        ed.setMessage(e.getMessage());
        ed.setUrl(req.getDescription(false));
        return ed;
    }
	@org.springframework.web.bind.annotation.ExceptionHandler(value = {DateIsNotFoundException.class})
    public ExceptionDetails handleDateIsNotFoundException(DateIsNotFoundException e, WebRequest req) {
        ExceptionDetails ed = new ExceptionDetails();
        ed.setMessage(e.getMessage());
        ed.setUrl(req.getDescription(false));
        return ed;
    }
	
	@org.springframework.web.bind.annotation.ExceptionHandler(value = {CustomerIdIsNotFound.class})
    public ExceptionDetails handleCustomerIdIsNotFound(CustomerIdIsNotFound e, WebRequest req) {
        ExceptionDetails ed = new ExceptionDetails();
        ed.setMessage(e.getMessage());
        ed.setUrl(req.getDescription(false));
        return ed;
    }
	
	@org.springframework.web.bind.annotation.ExceptionHandler(value = {EmployeeIdIsNotFound.class})
    public ExceptionDetails handleEmployeeIdIsNotFound(EmployeeIdIsNotFound e, WebRequest req) {
        ExceptionDetails ed = new ExceptionDetails();
        ed.setMessage(e.getMessage());
        ed.setUrl(req.getDescription(false));
        
        return ed;
    }
	@org.springframework.web.bind.annotation.ExceptionHandler(value = {IllegalArgumentException.class})
    public ExceptionDetails handleIllegalArgumentException(IllegalArgumentException e, WebRequest req) {
        ExceptionDetails ed = new ExceptionDetails();
        ed.setMessage(e.getMessage());
        ed.setUrl(req.getDescription(false));
        
        return ed;
    }
	@org.springframework.web.bind.annotation.ExceptionHandler(value = {StatusNotFoundException.class})
    public ExceptionDetails handleStatusNotFoundException(StatusNotFoundException e, WebRequest req) {
        ExceptionDetails ed = new ExceptionDetails();
        ed.setMessage(e.getMessage());
        ed.setUrl(req.getDescription(false));
        
        return ed;
    }
}
