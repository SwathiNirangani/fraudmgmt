
package com.fcm.krypton_bank.Contoller;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fcm.krypton_bank.Entity.CaseDetail;
import com.fcm.krypton_bank.Service.CaseDetailService;

@SpringBootTest

@AutoConfigureMockMvc
public class CaseDetailControllerTest {

	@Autowired
	MockMvc mockMvc;

	@MockBean
	private CaseDetailService caseDetailService;

	@Test
	public void testSave() throws Exception {
		LocalDate casedate =

				LocalDate.parse("2023-01-08");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); // Date caseDate=sdf.parse("2023-06-22");
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
		Date caseDate = sdf1.parse("2023-06-10"); // Date caseDate =
		Date.from(casedate.atStartOfDay(ZoneId.systemDefault()).toInstant()); //
		Date caseDate1 = sdf1.parse("2023-06-10"); // Date fraudDate =
		Date.from(casedate.atStartOfDay(ZoneId.systemDefault()).toInstant());
		//Date.from(frauddate.atStartOfDay(ZoneId.systemDefault()).toInstant());
		CaseDetail employee = CaseDetail.builder().cardHolderName("Kajal").accountType("saving").balPriorFraud(3456)
				.cardCancelled("yes").cardLost("yes").accountNumber("676375467").cardNumber("687468295")
				.transactionType("UPI").descAdditionalInfo("jhuf").caseDate(caseDate).fraudDate(caseDate1)
				.merchantName("kajal").possessionWhenFraudOccured("yes").pinShared("no").descTransactionOccured("dfege")
				.employee(null).customer(null).build();

		mockMvc.perform(MockMvcRequestBuilders.post("http://localhost:8092/api/casedetail/addcase")
				.contentType(MediaType.APPLICATION_JSON).content(new ObjectMapper().writeValueAsString(employee)))

				.andExpect(MockMvcResultMatchers.status().isOk()).andExpect(MockMvcResultMatchers.content().string("Case added successfully"));
	}

	@Test
	public void testFetchByCustomerId() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("http://localhost:8092/api/casedetail/details/2")
				.accept(MediaType.APPLICATION_JSON)).andExpect(MockMvcResultMatchers.status().isOk());

	}

	@Test
	public void testFetchByCustomerIdIfNonExisting() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("http://localhost:8092/api/casedetail/details/2")
				.accept(MediaType.APPLICATION_JSON)).andExpect(MockMvcResultMatchers.status().isOk());

	}
	@Test
	public void testUpdateCaseDetail() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.put("http://localhost:8092/api/casedetail/4")
				.accept(MediaType.APPLICATION_JSON)).andExpect(MockMvcResultMatchers.status().isOk());

	}
	@Test
	public void testUpdateCaseDetailAfterAssignation() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.put("http://localhost:8092/api/casedetail/status/4")
				.accept(MediaType.APPLICATION_JSON)).andExpect(MockMvcResultMatchers.status().isOk());

	}
	@Test
	public void teststatus() throws Exception {
		String status="reject";
		mockMvc.perform(MockMvcRequestBuilders.get("http://localhost:8092/api/casedetail/bystatus/reject")
				.accept(MediaType.APPLICATION_JSON)).andExpect(MockMvcResultMatchers.status().isOk());

	}
	
}
