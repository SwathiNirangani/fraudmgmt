package com.fcm.krypton_bank.Contoller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fcm.krypton_bank.Entity.Customer;
import com.fcm.krypton_bank.Service.CustomerService;
@SpringBootTest
@AutoConfigureMockMvc
public class CustomerControllerTest {
	
	@Autowired
	MockMvc mockMvc;
	@MockBean
	private CustomerService customerService;
	@Test
	public void testSave() throws Exception{
		 Customer employee= Customer.builder()
					.firstName("Sanjeev")
					.lastName("jhosi")
					.email("Sanjeev@gmail.com")
					.password("Sanjeev@23")
					.phoneNumber(9794239597L)	
					.gender("male")
					.address("UP")				
					.panCard("GUHJ786")
					.build();
		mockMvc.perform(MockMvcRequestBuilders
				.post("/api/customer/addcustomer")
				.contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(employee)))
				
		.andExpect(MockMvcResultMatchers.status().isOk())
		.andExpect(MockMvcResultMatchers.content().string(""));
	}
}
