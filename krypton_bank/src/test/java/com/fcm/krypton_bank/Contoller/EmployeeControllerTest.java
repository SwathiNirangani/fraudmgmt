package com.fcm.krypton_bank.Contoller;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;

import org.mockito.Mock;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fcm.krypton_bank.Entity.Employee;
import com.fcm.krypton_bank.Service.EmployeeService;

@SpringBootTest
@AutoConfigureMockMvc

//@WebMvcTest(EmployeeControllerTest.class)
public class EmployeeControllerTest {
	
	@Autowired
	MockMvc mockMvc;
	@MockBean
	private EmployeeService employeeService;
	//@Autowired private Employee employee;
	@Test 
	public void testFetchByIdEmployee() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders
				.get("/api/v2/employee/1")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isOk());
				
	}
	@Test
	public void testGetAllEmployee() throws Exception{
		mockMvc.perform(MockMvcRequestBuilders
				.get("/api/v2/employee")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isOk());
				
	}
	
	@Test
	public void testSave() throws Exception{
		 Employee employee= Employee.builder()
					.firstName("Sanjeev")
					.lastName("jhosi")
					.email("Sanjeev@gmail.com")
					.password("Sanjeev@23")
					.phoneno(9794239597L)	
					.gender("male")
					.employeeType("Reviewer")
					.noOfCasesHandling(1)
					.caseDetail(null)
					.build();
		mockMvc.perform(MockMvcRequestBuilders
				.post("/api/v2/employee/add")
				.contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(employee)))
				
		.andExpect(MockMvcResultMatchers.status().isOk())
		.andExpect(MockMvcResultMatchers.content().string(""));
				
	}
	
	
	@Test
	public void testDelete() throws Exception{
		mockMvc.perform(MockMvcRequestBuilders
				.delete("/api/v2/delete/1"))
				.andExpect(MockMvcResultMatchers.status().isOk());
		
	}
	

}
