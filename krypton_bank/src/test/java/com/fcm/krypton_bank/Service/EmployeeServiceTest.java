package com.fcm.krypton_bank.Service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.gen5.api.BeforeAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.fcm.krypton_bank.Entity.Employee;
import com.fcm.krypton_bank.Exception.EmailAlreadyExistException;
import com.fcm.krypton_bank.Repository.EmployeeRepository;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class EmployeeServiceTest {
	
	@InjectMocks
	EmployeeService employeeService;

	@Mock
	EmployeeRepository employeeRepository;

	@BeforeEach
	void setUp() {

	}

	@BeforeAll

	@Test
	public void fetchEmployeeById() {
		Employee emp = Employee.builder().firstName("Khushabu").lastName("singh").email("khushi@gmail.com")
				.password("Khushi@23").phoneno(9897969597L).gender("female").employeeType("Reviewer")
				.noOfCasesHandling(1).caseDetail(null).build();
		int empId = 1;
		// when(employeeRepository.findById((empId)).thenReturn(Optional.of(emp));
		when(employeeRepository.findById(empId)).thenReturn(Optional.of(emp));
		Employee employeeId = employeeService.findById(empId);
		assertNotNull(employeeId);
		assertEquals(emp.getFirstName(), employeeId.getFirstName());

	}

	@Test
	 void testSaveEmployee() throws Exception {
		
		 Employee employeeObj= Employee.builder()
					.firstName("Sanjeev")
					.lastName("jhosi")
					.email("shanjeev@gmail.com")
					.password("Sanjeev@23")
					.phoneno(9794239597L)	
					.gender("male")
					.employeeType("Reviewer")
					.noOfCasesHandling(1)
					.caseDetail(null)
					.build();
		 
		 when(employeeRepository.findByEmail(employeeObj.getEmail())).thenReturn(null);
		when( employeeRepository.save(employeeObj)).thenReturn(employeeObj);
         Employee returnEmpObj =	employeeService.save(employeeObj);
         Assertions.assertEquals(employeeObj.getFirstName(), returnEmpObj.getFirstName());

		 
		 
		

	 }
	@Test
	 void testSaveEmployeeIfExistEmail() {
		
		 Employee employeeObj= Employee.builder()
					.firstName("Sanjeev")
					.lastName("jhosi")
					.email("shanjeev@gmail.com")
					.password("Sanjeev@23")
					.phoneno(9794239597L)	
					.gender("male")
					.employeeType("Reviewer")
					.noOfCasesHandling(1)
					.caseDetail(null)
					.build();
		 when(employeeRepository.findByEmail(employeeObj.getEmail())).thenReturn(employeeObj);
		 assertThrows(EmailAlreadyExistException.class,()->{
			 employeeService.save(employeeObj);
		 });
	
	}
}
