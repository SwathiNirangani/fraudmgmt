package com.fcm.krypton_bank.Service;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.fcm.krypton_bank.Entity.CaseDetail;
import com.fcm.krypton_bank.Entity.Employee;
import com.fcm.krypton_bank.Exception.CustomerIdIsNotFound;
import com.fcm.krypton_bank.Exception.EmployeeIdIsNotFound;
import com.fcm.krypton_bank.Repository.CaseDetailRepository;
public class CaseDetailServiceTest {

	@Mock
    private CaseDetailRepository caseDetailRepo;

    @InjectMocks
    private CaseDetailService caseDetailService;
@BeforeEach
void setUp() {
	MockitoAnnotations.openMocks(this);
}
    @Test
    public void testSaveCaseDetail() {
        CaseDetail caseDetail=new CaseDetail();
        caseDetail.setCardCancelled("yes");
        caseDetail.setPossessionWhenFraudOccured("yes");
        caseDetail.setPinShared("no");
        caseDetail.setTransactionAmount(2000);
        caseDetail.setReviewerType("Reviewer");
        //caseDetail.setTransactionAmount(1001);
        MockitoAnnotations.initMocks(this);
        caseDetailService.save(caseDetail);
        assertEquals("Reviewer", caseDetail.getReviewerType());
        assertEquals("y", caseDetail.getCardCancelled());
        assertEquals("n", caseDetail.getPinShared());
        assertEquals("y",caseDetail.getPossessionWhenFraudOccured());
        
    }
    
    
    @Test
	public void testGetCaseDetailByCustomerId() {
    	int customerId=1;
    List<CaseDetail> existingCaseDetails=new ArrayList<>();
    existingCaseDetails.add(new CaseDetail());
    when(caseDetailRepo.findByCustomerCustomerId(customerId)).thenReturn(existingCaseDetails);
    List<CaseDetail> result=caseDetailService.getCaseDetailCustomerById(customerId);
    verify(caseDetailRepo).findByCustomerCustomerId(customerId);
    Assertions.assertEquals(existingCaseDetails, result);
    

	}
    @Test
    void testGetCaseDetailByCustomerIdIfNotExisting() {
    	int customerId=1;
    	when(caseDetailRepo.findByCustomerCustomerId(customerId)).thenReturn(new ArrayList<>());
    	Assertions.assertThrows(CustomerIdIsNotFound.class, ()->caseDetailService.getCaseDetailCustomerById(customerId));
    	verify(caseDetailRepo).findByCustomerCustomerId(customerId);
    }
    @Test
   	public void testGetCaseDetailByEmployeeId() {
       	int employeeId=1;
       	Employee employee=new Employee();
       List<CaseDetail> existingCaseDetails=new ArrayList<>();
       existingCaseDetails.add(new CaseDetail());
       when(caseDetailRepo.findByEmployee(employee)).thenReturn(existingCaseDetails);
       List<CaseDetail> result=caseDetailService.getCaseDetailEmployeeById(employee);
       verify(caseDetailRepo).findByEmployee(employee);
       Assertions.assertEquals(existingCaseDetails, result);
       

   	}
    @Test
    void testGetCaseDetailByEmployeeIdIfNotExisting() {
    	int employeeId=1;
    	Employee employee=new Employee();
    	when(caseDetailRepo.findByEmployee(employee)).thenReturn(new ArrayList<>());
    	Assertions.assertThrows(EmployeeIdIsNotFound.class, ()->caseDetailService.getCaseDetailEmployeeById(employee));
    	verify(caseDetailRepo).findByEmployee(employee);
    }
	
    @Test
	public void testUpdateCaseDetail() {
		// given
		int empId = 1;
		Employee employee = new Employee();
		employee.setEmpId(empId);
		CaseDetail caseDetail = new CaseDetail();
		caseDetail.setEmployee(employee);
		caseDetail.setTransactionAmount(2000);

		when(caseDetailRepo.findByEmployee(employee)).thenReturn(Arrays.asList(caseDetail));
		when(caseDetailRepo.save(caseDetail)).thenReturn(caseDetail);

		// when
		CaseDetail updatedCaseDetail = caseDetailService.updateCaseDetail(empId);

		// then
		verify(caseDetailRepo).findByEmployee(employee);
		verify(caseDetailRepo).save(caseDetail);
		assertEquals("Reviewer", updatedCaseDetail.getReviewerType());
	}
    
    
    @Test
    public void testUpdateCaseDetailAfterAssignationSuccess() {
      // given
      int empId = 1;
      Employee employee = new Employee();
      employee.setEmpId(empId);
      CaseDetail caseDetail = new CaseDetail();
      caseDetail.setEmployee(employee);
      caseDetail.setTransactionAmount(500);
      caseDetail.setTransactionType("DebitCard");
      caseDetail.setReviewerType("FraudcaseOfficer");
      caseDetail.setStatus("In progress");
      caseDetail.setCasePossibility("Accept");
      when(caseDetailRepo.findByEmployee(employee)).thenReturn(List.of(caseDetail));
      when(caseDetailRepo.save(caseDetail)).thenReturn(caseDetail);

      // then
      CaseDetail result = caseDetailService.updateCaseDetailAfterAssignation(empId);
      assertEquals(caseDetail, result);
    }
    @Test
    public void testUpdateCaseDetailAfterAssignationFailure() {
      // given
      int empId = 2;
      Employee employee = new Employee();
      employee.setEmpId(empId);

      // when
      when(caseDetailRepo.findByEmployee(employee)).thenReturn(List.of());

     
      Assertions.assertThrows(EmployeeIdIsNotFound.class,() ->{
    	  caseDetailService.updateCaseDetailAfterAssignation(empId);
      });
    }
    
    @Test
    public void testGetDetailByStatus() {
        String status = "new";
        List<CaseDetail> existingCaseRegister = new ArrayList<>();
        existingCaseRegister.add(new CaseDetail());
        when(caseDetailRepo.findByStatus(status)).thenReturn(existingCaseRegister);
        List<CaseDetail> result = caseDetailService.getCaseDetailsByStatus(status);
        verify(caseDetailRepo).findByStatus(status);
        assertEquals(existingCaseRegister, result);

 

    }
    
}
