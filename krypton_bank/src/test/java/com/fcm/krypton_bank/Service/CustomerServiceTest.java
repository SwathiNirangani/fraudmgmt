package com.fcm.krypton_bank.Service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;

import com.fcm.krypton_bank.Entity.Customer;
import com.fcm.krypton_bank.Repository.CustomerRepository;

public class CustomerServiceTest {
	@InjectMocks
	CustomerService customerService;

	@Mock
	CustomerRepository customerRepository;

	@BeforeEach
	void setUp() {
		MockitoAnnotations.openMocks(this);
	}
	@Test
	 public void testSaveCustomer() {
		
		 Customer customerObj= Customer.builder()
					.firstName("Sanjeev")
					.lastName("jhosi")
					.email("shanjeev@gmail.com")
					.password("Sanjeev@23")
					.phoneNumber(9794239597L)	
					.gender("male")
					.address("up")
					.panCard("HUO7525")
					.caseDetails(null)
					.build();
		 
		
		 Mockito.when(customerRepository.save(customerObj)).thenReturn(customerObj);
		    Assertions.assertEquals(customerObj, customerService.save(customerObj));
		 
		 
		

	 }

}
